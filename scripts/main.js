let users, offset = 0, usersToDisplay = 3, totalUsers = 0, activeRowIndex = 0

const usersContainer = document.getElementById('users_list')

// fetch api
fetch('https://jsonplaceholder.typicode.com/users').then(response => response.json()).then(data => {
  users = data
  totalUsers = users.length
})

const scrollEffect = () => {
  let scrolled = window.pageYOffset
  let parallax = document.querySelector('.parallax')
  // Image parallax
  const speed = 0.4
  // we can adjust the speed
  let coords = (scrolled * speed) + 'px'
  parallax.style.transform = `translateY(${coords})`


  // whether or not to display the new rows with data in it
  displayRows()
}

function displayRows () {
  let currentRow = document.getElementById(`row_${activeRowIndex}`)
  if (!currentRow) {

    // we create next row
    const usersRow = document.createElement('div')
    usersRow.className = `users_row`
    usersRow.id = `row_${activeRowIndex}`
    usersContainer.appendChild(usersRow)

    // scroll event will enter here only once per new row
    currentRow = usersRow
  } else {
    const rowOffsetTop = currentRow.offsetTop
    const rowHeight = currentRow.offsetHeight



    if (window.innerHeight + window.scrollY >= rowOffsetTop + rowHeight) {
      // when we are at the bottom of the row to be displayed data on
      displayUsers(users, currentRow)
    }
  }
}

function displayUsers (data, row, searchTerm) {

  // array for elements
  let elements = []

  // keeping track of total users currently displayed per this iteration (for this example it's 3 per row)
  let itemCounter = 0

  data.forEach((user, index) => {

    // we display only users that didn't show earlier
    if (index >= offset && offset < usersToDisplay) { // offset : 0, 3, 6

      // this is for staggering display of users
      let delay = 200 * (index % 3 + 1) // 200, 400, 600

      let userDiv = buildUserDiv(user,index)

      // delay for opacity
      userDiv.style.transitionDelay = delay + 'ms'
      elements.push(userDiv)

      // offset is a global var, we need to track it for the next iteration
      offset++

      // this usually results in 3 since we display 3 per row
      itemCounter++
    }
  })

  if(elements) {
    elements.forEach((userDiv) => {
      row.appendChild(userDiv).focus()
      userDiv.classList.add('fadeIn')
    })
  }

  if (usersToDisplay < totalUsers) {

      // this, combined with offset let's us regulate which groups of users should be displayed (0-3, 3-6, 6-9, ...)
      usersToDisplay += itemCounter

    // we prepare the next row to be filled with data when we scroll to it
    activeRowIndex++
  }

}

function buildUserDiv(user, index){
  let userElement = document.createElement('div')
  userElement.innerHTML = `
            <h3>User Nr. ${index + 1}</h3>
             <div class="info">
                <p><span>Name:</span> ${user.name} </p> 
                <p><span>Email Adress:</span> ${user.email}</p>
                <p><span>Phone Number:</span> ${user.phone}</p>
                <p><span>Street</span> ${user.address.street}</p>
                <p><span>City:</span> ${user.address.city}</p>
                <p><span>Zipcode:</span> ${user.address.zipcode}</p>
            </div>`

  userElement.classList.add('user')

  return userElement
}

window.addEventListener('scroll', scrollEffect)
